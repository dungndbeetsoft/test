<?php 
    /**
     * Khai bao hang gia tri
     */
    define( 'THEME_URL', get_stylesheet_directory());
    define( 'CORE', THEME_URL . "/core");

    function theme_setup() {
        register_nav_menu('topmenu',__( 'Menu chính' ));
        register_nav_menu('footer_menu',__( 'Menu Footer' ));
        add_theme_support('post-thumbnails');

        $sidebar = array(
            'name' => __('Main Sidebar', 'mytoppan'),
            'id' => 'main-sidebar',
            'description' => 'Default sidebar',
            'class' => 'main-sidebar',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>',
        );
        register_sidebar($sidebar);
    }
    add_action('init', 'theme_setup');
    /** Logo */

        require_once get_template_directory() . '/customizer.php';
    /**
/** 
  nhung file /core/init.php
 **/
    require_once( CORE . "/init.php");
/**
 menu
**/
if ( !function_exists('wp_menu') ) {
	function wp_menu($menu) {
		$menu = array(
			'theme_location' => $menu,
			'container' => '',
			'container_class' => $menu,
			'items_wrap' => '<ul id="%1$s" class="%2$s navbar-nav ml-auto">%3$s</ul>',
			'add_li_class'  => 'nav-item',
			'link_class'   => 'nav-link'
		);
		wp_nav_menu( $menu );
	}
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

function add_menu_link_class( $atts, $item, $args ) {
	if (property_exists($args, 'link_class')) {
	  $atts['class'] = $args->link_class;
	}
	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );

add_filter('nav_menu_item_args', function ($args, $item, $depth) {
    if ($args->theme_location == 'topmenu') {
        $title             = apply_filters('the_title', $item->title, $item->ID);
        $args->link_before = '<span data-hover="' . $title . '">';
        $args->link_after  = '</span>';
    }
    return $args;
}, 10, 3);
