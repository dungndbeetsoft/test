$('.nav-one').on('click', function() {
	$('.navtabs').addClass('nav-start');
  $('.navtabs').removeClass('nav-end');
  $('.navtabs').removeClass('nav-center');
});
$('.nav-two').on('click', function() {
	$('.navtabs').addClass('nav-center');
  $('.navtabs').removeClass('nav-start');
  $('.navtabs').removeClass('nav-end');
});
$('.nav-three').on('click', function() {
	$('.navtabs').addClass('nav-end');
  $('.navtabs').removeClass('nav-center');
  $('.navtabs').removeClass('nav-start');
});
$('.popup-one').on('click', function() {
	$('.nav-popup').addClass('nav-start');
  $('.nav-popup').removeClass('nav-end');
  $('.nav-popup').removeClass('nav-center');
});
$('.popup-two').on('click', function() {
	$('.nav-popup').addClass('nav-center');
  $('.nav-popup').removeClass('nav-start');
  $('.nav-popup').removeClass('nav-end');
});
$('.popup-three').on('click', function() {
	$('.nav-popup').addClass('nav-end');
  $('.nav-popup').removeClass('nav-center');
  $('.nav-popup').removeClass('nav-start');
});

$('.btn-toggler').on('click', function() {
  if ($('.group-toggler').hasClass('text-block')) {

    $('.group-toggler').removeClass('text-block');
    $('.group-toggler').addClass('text-none');
  }
  else {
    $('.group-toggler').addClass('text-block');
    $('.group-toggler').removeClass('text-none');
  }
    

});

var image = document.getElementById("js-btn-img");
function changeLanguage(){
    if (image.getAttribute('src') == "http://mytoppan.com/wp-content/themes/toppan/images/Group52@2x.png"){
        image.src = "http://mytoppan.com/wp-content/themes/toppan/images/close@2x.png";
    }
    else {
        image.src = "http://mytoppan.com/wp-content/themes/toppan/images/Group52@2x.png";
    }
}

// banner-slider 
var sync1 = $(".js-banner");
var sync2 = $(".js-thumbnail");

var thumbnailItemClass = '.owl-item';

var slides = sync1.owlCarousel({
	video:true,
    items:1,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    nav: false,
    dots: false
}).on('changed.owl.carousel', syncPosition);

function syncPosition(el) {
  $owl_slider = $(this).data('owl.carousel');
  var loop = $owl_slider.options.loop;

  if(loop){
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    if(current < 0) {
        current = count;
    }
    if(current > count) {
        current = 0;
    }
  }else{
    var current = el.item.index;
  }

  var owl_thumbnail = sync2.data('owl.carousel');
  var itemClass = "." + owl_thumbnail.options.itemClass;


  var thumbnailCurrentItem = sync2
  .find(itemClass)
  .removeClass("synced")
  .eq(current);

  thumbnailCurrentItem.addClass('synced');

  if (!thumbnailCurrentItem.hasClass('active')) {
    var duration = 300;
    sync2.trigger('to.owl.carousel',[current, duration, true]);
  }   
}
var thumbs = sync2.owlCarousel({
  items:4,
  loop:false,
  margin:10,
  autoplay:false,
  nav: true,
  dots: false,
  onInitialized: function (e) {
    var thumbnailCurrentItem =  $(e.target).find(thumbnailItemClass).eq(this._current);
    thumbnailCurrentItem.addClass('synced');
  },
})
.on('click', thumbnailItemClass, function(e) {
    e.preventDefault();
    var duration = 300;
    var itemIndex =  $(e.target).parents(thumbnailItemClass).index();
    sync1.trigger('to.owl.carousel',[itemIndex, duration, true]);
}).on("changed.owl.carousel", function (el) {
  var number = el.item.index;
  $owl_slider = sync1.data('owl.carousel');
  $owl_slider.to(number, 100, true);
});


// scroll to top

// var target = document.querySelector("footer");
// var scrollToTopBtn = document.querySelector(".scrollToTopBtn")
// var rootElement = document.documentElement

// function callback(entries, observer) {
//   entries.forEach(entry => {
//     if (entry.isIntersecting) {
//       scrollToTopBtn.classList.add("showBtn")
//     } else {
//       scrollToTopBtn.classList.remove("showBtn")
//     }
//   });
// }
// function scrollToTop() {
//   rootElement.scrollTo({
//     top: 0,
//     behavior: "smooth"
//   })
// }
// scrollToTopBtn.addEventListener("click", scrollToTop);
// let observer = new IntersectionObserver(callback);
// observer.observe(target);
var btn = $('.scrollToTopBtn');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('showBtn');
  } else {
    btn.removeClass('showBtn');
  }
});

btn.on('click', function(e) {
  // e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
}); 

$('.button-mb').on('click', function() {
	$('.submenu').toggleClass('show');

});


// modal contact-confirmation
$(document).ready(function(){
  $('#contact').validate({
    rules: {
      name: "required",
      email: {
        required: true,
        email: true
      },
      province: {
        required: true,
      },
      district: {
        required: true,
      },
      ward: {
        required: true,
      },
      phone: {
        required: true,
      },
      checkbox: {
        required: true,
      },
      radio: {
        required: true,
      }
    },

    messages: {
      email: "Please enter a valid email address",
      checkbox: "You must check at least 1 box",
      province: "You must choice one option",
      district: "You must choice one option",
      ward: "You must choice one option",
      radio: "You must check box",
    },
    submitHandler: function(form) {
      form.submit();
      window.location.href = "http://test-beetsoft.local/contact-confirmation/";
    }
  });
});


$(document).ready(function() {

  $.getJSON('https://provinces.open-api.vn/api/?depth=3', function(dataCity) {

    // select city
    $.each(dataCity, function (index, value) {
      $('#province').append('<option value="'+value.name+'">'+value.name+'</option>');
    });
    // select districts
    $('#province').on('change', function(){
      for(var i = 0; i < dataCity.length; i++) {
        if(dataCity[i].name == $(this).val()) {
          var districts = [];
          $.each(dataCity[i].districts, function (index, value) {
            districts.push('<option value="'+value.name+'">'+value.name+'</option>');
          });
          $('#district').html('<option value="">Select District</option>'+ districts);
        }
      }
      wards.push('<option value="">Select Ward</option>');
      $('#ward').html(wards);
    });

    // select wards
    var wards = [];
    $('#district').on('change', function(){
      if($(this).attr('value') == '') {
        wards.push('<option value="">Select Ward</option>');
        $('#ward').html(wards);
      }
      else {
        for(var i = 0; i < dataCity.length; i++) {
          for( var j = 0; j < dataCity[i].districts.length; j++) {
            if(dataCity[i].districts[j].name == $(this).val()) {
              var wards = [];
              $.each(dataCity[i].districts[j].wards, function (index, value) {
                wards.push('<option value="'+value.name+'">'+value.name+'</option>');
              });
              $('#ward').html('<option value="">Select Ward</option>' + wards);
            }
          }
        }
      }
    })
    
  })
  

  // Instead of listening to button click, always listen to form submit event
  $('#contact').submit(function(e) {
      e.preventDefault();

      // Create an array of objects called `formData`
      var formData = $(this).find(':input').map(function() {
        if($(this).attr('type') == 'checkbox') {
          if($(this).is(':checked')) {
            return {
              name: $(this).attr('name'),
              value: $(this).val()
            };
          }
        }
        else if($(this).attr('type') == 'radio') {
          if($(this).is(':checked')) {
            return {
              name: $(this).attr('name'),
              value: $(this).val()
            };
          }
        }
        else{ 
          return {
            name: $(this).attr('name'),
            value: $(this).val(),
          };
        }
      }).get();

      // Store it in localStorage
      localStorage.setItem('formData', JSON.stringify(formData));

  });
  // get data input, select
  var formDaTa = JSON.parse(localStorage.getItem('formData'));
  var values = '';
  var valuesSelect = '';
  $.each(formDaTa, function(i, datum) {

    // get data multiple checkbox
    if(datum.name == 'checkbox') {
      values += datum.value + ', ';
      var str = values.replace(/,\s*$/, "");
      $('.input-' + datum.name).val(str);
    }
    // get data multiple select
    else if((datum.name == 'ward') || (datum.name == 'district') || (datum.name == 'province')) {
      valuesSelect += datum.value + ', ';
      var str = valuesSelect.replace(/,\s*$/, "");
      var strAdd = str.split(",").reverse();
      $('.input-' + datum.name).val(strAdd);
    }

    // get data input, radio
    else {
      $('.input-' + datum.name).val(datum.value);
    }
  });

});
jQuery(function($) {
  $('.label .readonly').prop('readonly','true'); 
  $('#wpcf7-f22-o1 .wrap-button').append('<a onclick="history.back()" type="button" id="cancel" class="btn btn-danger">Cancel</a>');
});
document.addEventListener( 'wpcf7mailsent', function( event ) {
  location = 'http://test-beetsoft.local/contact-complete/';
}, false );


