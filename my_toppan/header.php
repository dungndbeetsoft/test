<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/float-label-css/1.0.2/float-label.css" integrity="sha512-GMyeFHXXqU8RubxiWiyZqDgyFOmR8+jfl9m80fruu/E0VgB+e5Okw23MIIvz/BMD9WnXa26p2qXLFDqXyyeImw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/float-label-css/1.0.2/float-label.min.css" integrity="sha512-do6mlRTj3wXyR+NteKUQe+lYsAbWeaKBF8G2xcojpoww7KrRvd4kLkqjjCP0WXj/ekjqUcYX8ZiZjEHDHJ4e9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/app.css">
    <?php wp_head(); ?>
    <!-- <script>
        (function(d) {
          var config = {
            kitId: 'cmx0avq',
            scriptTimeout: 3000,
            async: true
          },
          h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
        })(document);
    </script> -->
</head>
<body>
    <div id="xmas-popup" class="popup" href="#">
        <div class="popup-heading d-flex justify-content-between">
            <h3 class="popup-title">Search for Products</h3>
            <a href="#" class="close">
                <img src="<?php bloginfo('template_directory') ?>/images/Group 1219@2x.png" alt="">
            </a>
        </div>
        <div class="container form-wrapper">
            <div class="input-group align-items-center">
                <label for="">フリーワード検索</label>
                <input type="text" class="form-control" placeholder="紙パッケージ　高級感" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <a href="#" class="btn-search input-group-text" id="basic-addon2">
                    <img src="<?php bloginfo('template_directory') ?>/images/Group 38.svg" alt="">
                  </a>
                </div>
            </div>
        </div>
        <div class="container popup-content">
            <ul class="nav nav-tabs mt-5 flex-nowrap nav-popup nav-start">
                <li class="nav-item">
                  <a class="nav-link active product-tab popup-one" data-toggle="tab" aria-current="page" href="#popup-tab1">By product type</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link popup-two" data-toggle="tab" href="#popup-tab2">By industry</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link popup-three" data-toggle="tab" href="#popup-tab3">By purpose</a>
                </li>
            </ul>
            <div class="tab-content"> 
                <div class="tab-pane active" id="popup-tab1">
                    <div class="product-type">
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img ">
                                    <a href="#">
                                        <img src="<?php bloginfo('template_directory') ?>/images/Mask Group 113@2x.png" alt="">
                                    </a>
                                </div>
                                <p class="type-descript">Flexible Packaging</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img ">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 112@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Barrier <br> Film</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 114@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Folding <br> Cartons</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 115@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Composite Containers <br> for Liquids</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 116@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Plastic Products</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 117@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Corrugated Board</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 118@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Performance Materials and Energy</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 119@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">BPO/ <br> Filling</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 120@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Digital transformation</p>
                            </div>
                        </div>
                        <div class="type-list">
                            <div class="type-item d-flex align-items-center">
                                <div class="type-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 121@2x.png" alt=""></a>
                                </div>
                                <p class="type-descript">Others</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="popup-tab2">
                    <div class="product-industry">
                        <div class="industry-list">
                            <div class="industry-item d-flex align-items-center">
                                <div class="industry-img ">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 124@2x.png" alt=""></a>
                                </div>
                                <p class="industry-descript">Medical and Pharmaceutical Goods</p>
                            </div>
                        </div>
                        <div class="industry-list">
                            <div class="industry-item d-flex align-items-center">
                                <div class="industry-img ">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 125@2x.png" alt=""></a>
                                </div>
                                <p class="industry-descript">Foods & Confectionery</p>
                            </div>
                        </div>
                        <div class="industry-list">
                            <div class="industry-item d-flex align-items-center">
                                <div class="industry-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 126@2x.png" alt=""></a>
                                </div>
                                <p class="industry-descript">Toiletries</p>
                            </div>
                        </div>
                        <div class="industry-list">
                            <div class="industry-item d-flex align-items-center">
                                <div class="industry-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 122@2x.png" alt=""></a>
                                </div>
                                <p class="industry-descript">Beverages</p>
                            </div>
                        </div>
                        <div class="industry-list">
                            <div class="industry-item d-flex align-items-center">
                                <div class="industry-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 123@2x.png" alt=""></a>
                                </div>
                                <p class="industry-descript">Industrial Materials</p>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="tab-pane fade" id="popup-tab3">
                    <div class="product-purpose">
                        <div class="purpose-list">
                            <div class="purpose-item d-flex align-items-center">
                                <div class="purpose-img ">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 129@2x.png" alt=""></a>
                                </div>
                                <p class="purpose-descript">User-Friendly Design</p>
                            </div>
                        </div>
                        <div class="purpose-list">
                            <div class="purpose-item d-flex align-items-center">
                                <div class="purpose-img ">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 130@2x.png" alt=""></a>
                                </div>
                                <p class="purpose-descript">Providing Information & Designs</p>
                            </div>
                        </div>
                        <div class="purpose-list">
                            <div class="purpose-item d-flex align-items-center">
                                <div class="purpose-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 131@2x.png" alt=""></a>
                                </div>
                                <p class="purpose-descript">Folding <br> Cartons</p>
                            </div>
                        </div>
                        <div class="purpose-list">
                            <div class="purpose-item d-flex align-items-center">
                                <div class="purpose-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 128@2x.png" alt=""></a>
                                </div>
                                <p class="purpose-descript">Earth-Friendly Materials</p>
                            </div>
                        </div>
                        <div class="purpose-list">
                            <div class="purpose-item d-flex align-items-center">
                                <div class="purpose-img">
                                    <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 127@2x.png" alt=""></a>
                                </div>
                                <p class="purpose-descript">Digitalizing processes</p>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <header class="header sticky-top">
        <div class="container-fluid header-content">
            <div class="row">
              <nav class="navbar navbar-expand-xl w-100 justify-content-between ml-md-auto p-0">
                <?php if ( get_theme_mod( 'carrental_logo' ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header-logo p-0 d-flex align-items-center" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                    <img src="<?php echo get_theme_mod( 'carrental_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    Packaging <br> Solutions
                </a>
                <?php else : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header-logo p-0 d-flex align-items-center" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                    <img src="<?php echo get_template_directory_uri() ?>/images/logo@2x.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    Packaging <br> Solutions
                </a>
                <?php endif; ?>
                <div class="group-toggler text-block align-items-center" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="nav-item h-100" id="language-hide">
                        <a class="nav-link nav-language h-100 d-flex align-items-center"  href="#">
                          <span >日本語</span>
                        </a>
                    </div>
                    <button class="navbar-toggler btn-toggler text-white"  type="button" data-toggle="collapse" data-target="#header--navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <img id="js-btn-img" onclick = "changeLanguage()" src="<?php bloginfo('template_directory') ?>/images/Group52@2x.png" alt="">
                    </button>
                </div>
                <div class="collapse navbar-collapse header-navbar h-100" id="header--navbar">
                    <?php wp_nav_menu( 
                        array( 
                            'theme_location' => 'topmenu', 
                            'container' => 'false', 
                            'menu_id' => 'top-menu', 
                            'menu_class' => 'top-menu navbar-nav navbar-mb ml-auto align-lg-items-center',
                            'add_li_class'  => 'nav-item h-100',
                            'link_class'   => 'nav-link h-100 d-flex align-items-center justify-content-between',
                            ) 
                            
                    ); ?>
                    <ul class="navbar-nav navBar-mb">
                        <li class="nav-item d-none d-xl-block h-100 nav-contact" >
                            <a class="nav-link h-100 d-flex align-items-center"  href="/contact-form/">
                                Contact Us
                            </a>
                        </li>
                        <li class="nav-item h-100 d-inline-block">
                            <a class="nav-link d-none nav-language h-100 d-xl-flex align-items-center"  href="#">
                                <span data-hover="English">日本語</span>
                            </a>
                            <a class="nav-link d-flex nav-language language-mb h-100 d-xl-none align-items-center"  href="#">
                                <span >English</span>
                            </a>
                        </li>
                        <li class="d-block d-xl-none nav-item h-100 text-center nav-contactMB">
                            <button class="btn btn--primary btn-contact">
                                <span class="d-flex align-items-center justify-content-center">
                                        Contact Us
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                        <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                        <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                    </svg>
                                </span>
                            </button>
                        </li>
                    </ul>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
    </header>