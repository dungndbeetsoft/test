<div class="blog-list">
    <?php 
        $args = array(
            'post_status' => 'publish', //chỉ lấy những bài viết được publish
            'post_type' => 'post', // lấy những bài viết thuộc post, nếu lấy những bài viết trong 'trang' thì để là page
            'showposts' => 3, // số lượng bài viết
        );
    ?>
    <?php $getposts = new WP_query($args); ?>
    <?php global $wp_query; $wp_query->in_the_loop = true; ?>
    <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
        <div class="blog-item">
            <div class="blog-img">
                <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail(get_the_id(), 'full', array('class'=> 'thumbnail')); ?></a>
            </div>
            <div class="blog-status d-flex justify-content-between">
                <div class="d-flex align-items-center">
                    <div class="status-circle"></div>
                    <span class="status-title">News release</span>
                </div>
                <div class="blog-time">
                    <span><?php echo get_the_date('m-d-y'); ?></span>
                </div>
            </div>
            <a href="<?php the_permalink(); ?>" class="blog-title">
                <h6 >
                    <?php the_title(); ?>
                </h6>
            </a>
            <?php the_excerpt(); ?>
        </div>
    <?php endwhile; wp_reset_postdata(); ?>
</div>