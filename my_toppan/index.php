<?php get_header(); ?>
    <main>
        <section class="banner">
            <div class="container-fluid position-relative">
                <div class="js-banner owl-carousel owl-theme">
                    <?php 
                        global $redux_demo;
                        if ( !empty( $redux_demo['slides'] ) ) {
                            foreach ( $redux_demo['slides'] as $image ) {
                            echo '
                                <div class="item">
                                    <div class="banner-main flex-column flex-lg-row">
                                        <div class="banner-img">
                                            <a href="#">
                                                <img src="'.$image['image'].'" /> 
                                            </a>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="banner-content">
                                            <h2 class="banner-title">
                                                '.$image['title'].'
                                            </h2>
                                            <p class="banner-descript">
                                                '.$image['description'].'
                                            </p>
                                        </div> 
                                    </div>
                                </div>';
                            }
                        }
                    ?> 
                </div>
                <div class="thumbnail js-thumbnail owl-carousel d-none d-lg-flex position-absolute">
                    <?php 
                        if ( !empty( $redux_demo['slides'] ) ) {
                            foreach ( $redux_demo['slides'] as $image ) {
                                echo 
                                '<div class="thumbnail-item item">
                                    <a href="#">
                                        <img 
                                            src=" '.$image['thumb'].'"
                                            alt="thumbnail"
                                        >
                                    </a>
                                </div>';
                            }
                        }
                    ?>
                </div>
            </div>
            <div id="scroll-down">
                <span id="scroll-title">
                  Scroll
                </span>
            </div>
        </section>
        <section class="product">
            <h1 class="product-heading">
                PRODUCT
                <span class="product-smallHeading">
                    Our Products
                </span>
            </h1>
            <div class="container">
                <ul class="nav nav-tabs mt-5 flex-nowrap navtabs nav-start">
                    <li class="nav-item">
                      <a class="nav-link active product-tab nav-one" data-toggle="tab" aria-current="page" href="#tab1">By product type</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link nav-two" data-toggle="tab" href="#tab2">By industry</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link nav-three" data-toggle="tab" href="#tab3">By purpose</a>
                    </li>
                </ul>
                <div class="tab-content"> 
                    <div class="tab-pane active" id="tab1">
                        <div class="product-type">
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img ">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 113@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Flexible Packaging</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img ">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 112@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Barrier <br> Film</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 114@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Folding <br> Cartons</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 115@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Composite Containers <br> for Liquids</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 116@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Plastic Products</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 117@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Corrugated Board</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 118@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Performance Materials and Energy</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 119@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">BPO/ <br> Filling</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 120@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Digital transformation</p>
                                </div>
                            </div>
                            <div class="type-list">
                                <div class="type-item d-flex align-items-center">
                                    <div class="type-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 121@2x.png" alt=""></a>
                                    </div>
                                    <p class="type-descript">Others</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab2">
                        <div class="product-industry">
                            <div class="industry-list">
                                <div class="industry-item d-flex align-items-center">
                                    <div class="industry-img ">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 124@2x.png" alt=""></a>
                                    </div>
                                    <p class="industry-descript">Medical and Pharmaceutical Goods</p>
                                </div>
                            </div>
                            <div class="industry-list">
                                <div class="industry-item d-flex align-items-center">
                                    <div class="industry-img ">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 125@2x.png" alt=""></a>
                                    </div>
                                    <p class="industry-descript">Foods & Confectionery</p>
                                </div>
                            </div>
                            <div class="industry-list">
                                <div class="industry-item d-flex align-items-center">
                                    <div class="industry-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 126@2x.png" alt=""></a>
                                    </div>
                                    <p class="industry-descript">Toiletries</p>
                                </div>
                            </div>
                            <div class="industry-list">
                                <div class="industry-item d-flex align-items-center">
                                    <div class="industry-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 122@2x.png" alt=""></a>
                                    </div>
                                    <p class="industry-descript">Beverages</p>
                                </div>
                            </div>
                            <div class="industry-list">
                                <div class="industry-item d-flex align-items-center">
                                    <div class="industry-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 123@2x.png" alt=""></a>
                                    </div>
                                    <p class="industry-descript">Industrial Materials</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="tab-pane fade" id="tab3">
                        <div class="product-purpose">
                            <div class="purpose-list">
                                <div class="purpose-item d-flex align-items-center">
                                    <div class="purpose-img ">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 129@2x.png" alt=""></a>
                                    </div>
                                    <p class="purpose-descript">User-Friendly Design</p>
                                </div>
                            </div>
                            <div class="purpose-list">
                                <div class="purpose-item d-flex align-items-center">
                                    <div class="purpose-img ">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 130@2x.png" alt=""></a>
                                    </div>
                                    <p class="purpose-descript">Providing Information & Designs</p>
                                </div>
                            </div>
                            <div class="purpose-list">
                                <div class="purpose-item d-flex align-items-center">
                                    <div class="purpose-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 131@2x.png" alt=""></a>
                                    </div>
                                    <p class="purpose-descript">Folding <br> Cartons</p>
                                </div>
                            </div>
                            <div class="purpose-list">
                                <div class="purpose-item d-flex align-items-center">
                                    <div class="purpose-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 128@2x.png" alt=""></a>
                                    </div>
                                    <p class="purpose-descript">Earth-Friendly Materials</p>
                                </div>
                            </div>
                            <div class="purpose-list">
                                <div class="purpose-item d-flex align-items-center">
                                    <div class="purpose-img">
                                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/Mask Group 127@2x.png" alt=""></a>
                                    </div>
                                    <p class="purpose-descript">Digitalizing processes</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        <section class="solution">
            <div class="container">
                <div class="solution-main flex-column-reverse flex-lg-row">
                    <div class="solution-content">
                        <h2 class="solution-heading">
                            TOPPAN
                            <br>
                            Total Packaging
                            <br>
                            Solutions
                        </h2>
                        <p class="solution-descript">We provide optimal solutions matched to various 
                            settings related to our customers’ products, 
                            including product planning, development, and promotion.
                        </p>
                        <button class="btn btn--primary">
                            <span class="d-flex align-items-center justify-content-center">
                                Total Solutions
                            </span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                    <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                    <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                </svg>
                            </span>
                        </button>
                    </div>
                    <div class="solution-img">
                        <a href="#">
                            <img class="img-pc" src="<?php bloginfo('template_directory') ?>/images/solution-img@2x.png" alt="solution">
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="corevalue">
            <div class="corevalue-main flex-column flex-lg-row">
                <div class="corevalue-img ">
                    <img src="<?php bloginfo('template_directory') ?>/images/Group 3335@2x.png" alt="corevalue">
                </div>
                <div class="corevalue-content">
                    <h2 class="corevalue-heading">
                        TOPPAN
                        <br>
                        Packaging
                        <br>
                        Core Value
                    </h2>
                    <p class="corevalue-descript">To help achieve a sustainable society, 
                        we are creating value that considers the future of people, society, and the planet.
                    </p>
                    <div class="button-wrapper">
                        <button class="btn btn--primary d-flex align-items-center justify-content-center">
                            <span class="d-flex align-items-center justify-content-center">
                                Toppan’s Sustainability Initiatives
                            </span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                    <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                    <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                </svg>
                            </span>
                        </button>
                    </div>
                    <div class="button-wrapper">
                        <button class="btn btn-small">
                            <span class="d-flex align-items-center justify-content-center">
                                <span class="btn-text-one">Value for people</span>
                                <span class="btn-text-two">SMART LIFE VALUE</span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                        <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                        <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                    </svg>  
                                </span>
                            </span>
                        </button>
                    </div>
                    <div class="button-wrapper">  
                        <button class="btn btn-small">
                            <span class="d-flex align-items-center justify-content-center">
                                <span class="btn-text-one">Value for society</span>
                                <span class="btn-text-two">SOCIAL VALUE</span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                        <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                        <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                    </svg>
                                </span>
                            </span>
                        </button>
                    </div>
                    <div class="button-wrapper">
                        <button class="btn btn-small">
                            <span class="d-flex align-items-center justify-content-center">
                                <span class="btn-text-one">Value for the Earth</span>
                                <span class="btn-text-two">SUSTAINABLE VALUE</span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                        <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                        <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                    </svg>
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <h3 class="blog-heading">
                        News <br class="d-block d-lg-none"> Release
                    </h3>
                    <button class="btn btn--primary">
                        <span class="d-flex align-items-center justify-content-center">
                            News Release
                        </span>
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                            </svg>
                        </span>
                    </button>
                </div>
                <?php get_template_part('newrelease'); ?>
            </div>
        </section>
        <section class="location">
            <div class="container">
                <div class="location-main">
                    <h3 class="location-heading">Locations</h3>
                    <p class="location-descript">
                        Both within Japan and abroad, 
                        we utilize our package production and 
                        sales network to provide solutions aimed at helping our customers with 
                        global procurement and production in the optimal location.
                    </p>
                    <div class="button-wrapper text-center text-xl-right">
                        <button class="btn btn--primary">
                            <span class="d-flex align-items-center justify-content-center">
                                Locations
                            </span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                    <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#121212"/>
                                    <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#121212" stroke-width="2"/>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </section>
        <section class="space">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                        <a href="#"><img src="<?php bloginfo('template_directory') ?>/images/img@2x.png" alt=""></a>
                    </div>
                    <div class="col-xl-6">
                        <h5 class="space-heading">Co-Creation Space “ L･IF･E ”</h5>
                        <p class="space-descript mb-0">
                            This is a co-creation space showcasing Toppan’s packaging, décor materials, 
                            and wide-ranging other solutions.
                        </p>
                        <p class="space-descript">
                            We shape and create the future of “life” together with customers based on 
                            ideas beginning with “if.”
                        </p>
                        <div class="btn-wrapper text-right">
                            <button class="btn btn-small">
                                <span class="d-flex align-items-center justify-content-center">
                                    L･IF･E website
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#0c4947" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#0c4947" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#0c4947" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="contact">
            <div class="container">
                <div class="contact-main">
                    <p class="contact-title">Toppan provides a wide range of solutions for packaging.
                        Let us know what you need.</p>
                    <div class="text-xl-right text-center">
                        <button class="btn btn-small">
                            <span class="d-flex align-items-center justify-content-center">
                                Contact us
                            </span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" id="arrow_r" width="18" height="8" viewBox="0 0 18 8">
                                    <path id="Polygon_8" data-name="Polygon 8" d="M4,0,8,6H0Z" transform="translate(18) rotate(90)" fill="#ffffff"/>
                                    <line id="Line_214" data-name="Line 214" x2="14" transform="translate(0 4)" fill="none" stroke="#ffffff" stroke-width="2"/>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </section>
        <section class="partner">
            <div class="container">
                <h6 class="related-business text-center text-xl-left">Related business</h6>
                <div class="partner-top">
                    <div class="partner-list">
                        <div class="partner-item">
                            <a href="#">
                                <img src="<?php bloginfo('template_directory') ?>/images/top_bnr1@2x.png" alt="">
                            </a>
                        </div>
                        <div class="partner-item">
                            <a href="#">
                                <img src="<?php bloginfo('template_directory') ?>/images/top_bnr2@2x.png" alt="">
                            </a>
                        </div>
                        <div class="partner-item">
                            <a href="#">
                                <img src="<?php bloginfo('template_directory') ?>/images/top_bnr3@2x.png" alt="">
                            </a>
                        </div>
                        <div class="partner-item">
                            <a href="#">
                                <img src="<?php bloginfo('template_directory') ?>/images/top_bnr4@2x.png" alt="">
                            </a>
                        </div>
                        <div class="partner-item">
                            <a href="#">
                                <img src="<?php bloginfo('template_directory') ?>/images/top_bnr5@2x.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <h6 class="related-business text-center text-xl-left">Affiliated Companies</h6>
                <div class="partner-bottom">
                    <div class="partner-list">
                        <div class="partner-item">
                            <button class="btn btn-small">
                                <span class="d-flex text-left align-items-center justify-content-between">
                                    Toppan Plastic Co., Ltd.
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#02B9A4" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                        <div class="partner-item">
                            <button class="btn btn-small">
                                <span class="">
                                    Toppan Packaging Service<br class="d-none d-lg-block"> Co., Ltd.
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#02B9A4" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                        <div class="partner-item">
                            <button class="btn btn-small">
                                <span class="d-flex text-left align-items-center justify-content-between">
                                    Toppan Prosprint Co., Ltd.
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#02B9A4" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                        <div class="partner-item">
                            <button class="btn btn-small">
                                <span class="d-flex text-left align-items-center justify-content-between">
                                    Toppan Cosmo, Inc.
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#02B9A4" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                        <div class="partner-item">
                            <button class="btn btn-small">
                                <span class="d-flex text-left align-items-center justify-content-between">
                                    T&T Enertechno Co., Ltd.
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#02B9A4" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                        <div class="partner-item">
                            <button class="btn btn-small">
                                <span class="d-flex text-left align-items-center justify-content-between">
                                    Toppan Infomedia Co., Ltd.
                                </span>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="icon_e:link" transform="translate(0.5 0.5)">
                                          <path id="Path_1" data-name="Path 1" d="M5,5V0H0" transform="translate(8 0.001)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <line id="Line_28" data-name="Line 28" y1="5" x2="5" transform="translate(7.5 0.5)" fill="none" stroke="#02B9A4" stroke-width="1"/>
                                          <path id="Path_5056" data-name="Path 5056" d="M392.345,650.7v6h-13v-13h6" transform="translate(-379.345 -643.697)" fill="none" stroke="#02B9A4" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn scrollToTopBtn p-0">
                <img src="<?php bloginfo('template_directory') ?>/images/btn_top.svg" alt="">
            </button>
        </section>
    </main>
<?php get_footer(); ?>