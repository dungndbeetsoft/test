<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage beetsoft
 * @since 1.0
 * @version 1.0
 * Template Name: Contact Complete
 */

get_header(); ?>


<main class="l-site__main" role="main" style="padding-top: 8em; padding-bottom: 5.5em;">
    <div class="container">
        <div class="row">
            <div class="col p-contact__main">
                <h1>Thank you for your message. It has been sent.</h1>
                <a href="/contact-form/">back to contact</a>
            </div>
        </div>
    </div>
</main>
<?php
wp_reset_postdata();
?>

<?php get_footer();
