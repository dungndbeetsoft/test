<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage beetsoft
 * @since 1.0
 * @version 1.0
 * Template Name: Contact
 */

get_header(); ?>


<main class="l-site__main" role="main" style="padding-top: 8em; padding-bottom: 5.5em;">
    <div class="container">
        <div class="row">
            <div class="col p-contact__main">
                <form action="" method="POST" id="contact" class="form-contact init" >
                    <div class="wrap-form">
                        <span class="">
                            <select name="province" id="province">
                                <option value="">Select City</option>
                            </select>
                        </span>
                        <span class="">
                            <select name="district" id="district">
                                <option value="">Select District</option>
                            </select>
                        </span>
                        <span class="">
                            <select name="ward" id="ward">
                                <option value="">Select Ward</option>
                            </select>
                        </span>
                    </div>
                    <div class="wrap-form">
                        <span class="">
                            <input type="text" id="name" name="name" size="40" class="name" placeholder="Name" required/>
                        </span>
                        <span class="">
                            <input type="email" id="email" name="email" size="40" class="email"  placeholder="Email" required/>
                        </span>
                    </div>
                    <div class="wrap-form">
                        <span class="">
                            <input type="tel" id="phone" name="phone" size="40" class="phone"  placeholder="Phone Number" required/>
                        </span>
                    </div>
                    <div class="wrap-form">
                        <span class="">
                            <textarea name="mes" cols="40" rows="10" class="message" placeholder="Write a message"></textarea>
                        </span>
                    </div>
                    <div class="flex">
                        <input type="checkbox" id="vehicle1" name="checkbox" value="Merc">
                        <label for="vehicle1">Merc</label><br>
                        <input type="checkbox" id="vehicle2" name="checkbox" value="Poscher">
                        <label for="vehicle2">Poscher</label><br>
                        <input type="checkbox" id="vehicle3" name="checkbox" value="Vinfast">
                        <label for="vehicle3">Vinfast</label><br>
                        <input type="checkbox" id="vehicle4" name="checkbox" value="Mclare">
                        <label for="vehicle4">Mclare</label><br>
                        <input type="checkbox" id="vehicle5" name="checkbox" value="Bently">
                        <label for="vehicle5">Bently</label><br>
                    </div>
                    <div class="flex">
                        <input type="radio" id="vehicle6" name="radio" value="Merc">
                        <label for="vehicle6">Merc</label><br>
                        <input type="radio" id="vehicle7" name="radio" value="Poscher">
                        <label for="vehicle7">Poscher</label><br>
                        <input type="radio" id="vehicle8" name="radio" value="Vinfast">
                        <label for="vehicle8">Vinfast</label><br>
                    </div>
                    <div class="wrap-button">
                        <input name="form_click" value="Submit" type="submit" id="js-submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<?php
wp_reset_postdata();
?>

<?php get_footer();
