<footer class="footer">
      <div class="container">
        <nav class="navbar footer-content align-items-start align-items-md-center flex-column-reverse flex-xl-row">
            <div class="footer-left d-flex flex-column">
                <span>Copyright (c) 2020 TOPPAN PRINTING CO., LTD.</span>
                <span>凸版印刷株式会社　生活・産業事業本部による、公式HPです。</span>
            </div>
            <div class="footer-right ml-xl-auto m-0">
              <?php wp_nav_menu( 
                  array( 
                    'theme_location' => 'footer_menu', 
                    'container' => 'div', 
                    'container_class' => 'main-nav',
                    'menu_id' => 'footer-menu', 
                    'menu_class' => 'footer-menu navbar-nav flex-row ml-auto',
                    'add_li_class'  => 'nav-item',
                    'link_class'   => 'nav-link',
                  ) 
                ); 
              ?>
            </div>
          </nav>
      </div>
    </footer>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script   src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js" integrity="sha512-gY25nC63ddE0LcLPhxUJGFxa2GoIyA5FLym4UJqHDEMHjp8RET6Zn/SHo1sltt3WuVtqfyxECP38/daUc/WVEA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/js/app.js"></script>
    <?php wp_footer(); ?>
</body>
</html>