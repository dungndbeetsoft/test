<?php 
        function carrental_customize_register( $wp_customize ) {
            $wp_customize->add_setting( 'carrental_logo' ); // Thêm cài đặt cho trình tải lên logo
            // Thêm kiểm soát cho trình tải lên logo (trình tải lên thực tế)
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'carrental_logo', array(
                'label'    => __( 'Upload Logo (replaces text)', 'carrental_logo' ),
                'section'  => 'title_tagline',
                'settings' => 'carrental_logo',
            ) ) );
        }
        add_action( 'customize_register', 'carrental_customize_register' );
        ?>